class Moradia:
    def __init__(self, area: float, endereco: str, numero: int, preco_imovel: float, tipo ='Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo

    def gerar_relatorio_preco(self):
        return self.preco_imovel/self.area


class Apartamento(Moradia):
    tipo = 'Apartamento'
    def __init__(self, area, endereco, numero, preco_imovel, preco_condominio: float, num_apt: int, num_elevadores: int):
        super(Apartamento, self).__init__(area, endereco, numero, preco_imovel, tipo=self.tipo)
        self.area = area
        self.endereco = endereco
        self.preco_imovel = preco_imovel
        self.preco_condominio = preco_condominio
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
        self.andar = int(str(num_apt)[0]) if num_apt < 100 else int(str(num_apt)[:2]) if num_apt < 1000 else int(str(num_apt)[:3])

    def gerar_relatorio(self):
        super(Apartamento, self).gerar_relatorio_preco()
        return f'{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} - preco por m²: R$ {self.gerar_relatorio_preco()}'
